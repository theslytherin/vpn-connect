# vpn-connect

A stupid shell script that connects to IITK Fortinet VPN.
In case you have not guessed, you need to be a bonafide student of IITK to be able to use this. In other words, if you do not have a CC ID, you should probably go back to scrolling Instagram.

## Getting started
Getting started should be a child's play but because I do not presume any level of intellect, here goes nothing.
<ol>
	<li>Clone the repository. </li>
	<li>Fire up the terminal in the cloned folder</li>
	<li>Type <em>sh vpn-connect.sh</em> (you can obviously use wildcards)</li>
	<li>Follow the prompts.</li>
</ol>

## Storytime
If you, too, have a lot of time on your hands, here is the completely unneccessary story behind this shell script. IITK has an internal network, and Y21s have an online semester. Sad. Students need to connect to the internal network to access really important things like the magnet link of the latest Modern-Family episode and some useless things like the End-Sem results. The good people in the Computer Center are mindful of this fact and provide clear instructions to help students connect to the VPN. But sadly, they assume everyone uses Windows. To be fair, they do have a guide for Linux users, but the linux client is hopelessly outdated. I do not want to key in the gateway, the port ,and my credentials everytime a new episode releases just because the client does have any provision for storing them. Fortunately, linux comes with a sexy command line fortinet client (atleast Fedora does) and writing shell scripts is really easy. The shell script is a result of 10 minutes of aimless hacking and 5 minutes of focused coding (basically my entire attention span).

## Screenshots (because why would you trust a random guy on the internet)
![A really cool screenshot](vpnss.png "A low-res screenshot of me trying to present a POC. ")
